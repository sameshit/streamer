//
//  FileStreamer.cpp
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "FileStreamer.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

FileStreamer::FileStreamer(CoreObject *core)
:Streamer(core),_header_parsed(false)
{
    _connect_task.OnTimeTask.Attach(this,&FileStreamer::ProcessConnect);
    OnTrackerConnect.Attach(this, &FileStreamer::ProcessTrackerConnected);
    OnTrackerDisconnect.Attach(this, &FileStreamer::ProcessTrackerDisconnected);
    OnPeerConnect.Attach(this, &FileStreamer::ProcessPeerConnected);
    OnPeerDisconnect.Attach(this, &FileStreamer::ProcessPeerDisconnected);

    fast_new(_live_reader,_core);
    _live_reader->OnVideoHeader.Attach(this, &FileStreamer::ProcessVideoHeader);
    _live_reader->OnVideoPacket.Attach(this, &FileStreamer::ProcessVideoPacket);

    _read_thread = _core->GetThread();
    _read_thread->OnThread.Attach(this,&FileStreamer::ProcessThread);

//    BufferSettings()->log_clearing = true;
}

FileStreamer::~FileStreamer()
{
    _core->ReleaseThread(&_read_thread);

    fast_delete(_live_reader);
}

void FileStreamer::ProcessTrackerConnected(MSession *session)
{
    LOG_INFO("Connected to tracker "<<PRINT_SESSION_ADDR(session));
}

void FileStreamer::ProcessTrackerDisconnected(MSession *session)
{
    LOG_INFO("Tracker "<<PRINT_SESSION_ADDR(session)<<" disconnected with reason: "
        <<session->GetDisconnectReason().str()<< ". Trying to reconnect in 2 seconds...");

    _connect_task.SetTimeout(2000);
    _core->GetScheduler()->Add(&_connect_task);
}

void FileStreamer::ProcessPeerConnected(MSession *session)
{
    LOG_INFO("Peer "<<PRINT_SESSION_ADDR(session) << " connected");

//    session->SetConnectionTimeout(1000*60*60);
}

void FileStreamer::ProcessPeerDisconnected(MSession *session)
{
    LOG_INFO("Peer "<<PRINT_SESSION_ADDR(session) << " disconnected with reason: "<<session->GetDisconnectReason().str());
}

void FileStreamer::ProcessConnect()
{
    if (!ConnectToTracker(_tracker_ip.c_str(), _tracker_port,_stun1_port,_stun2_ip,_stun2_port))
    {
        LOG_ERROR("Error while connecting to tracker: "<<COErr::Get());
        return;
    }

    if (!CreateStream(_stream_name))
    {
        LOG_ERROR("Couldn't create stream with name "<<_stream_name<<", with reason: "<<COErr::Get() );
        return;
    }
}

bool FileStreamer::ReadFile(const char *filename)
{
    uint64_t total_size,pos;
    string buf;
    FILE *f;

    f = fopen(filename, "r");
    if (f == NULL)
    {
        LOG_ERROR("Couldn't open file "<<filename<<". Errno: "<<errno);
        return false;
    }

    fseek(f, 0L, SEEK_END);
    total_size = ftell(f);
    fseek(f, 0L, SEEK_SET);

    if (total_size == 0)
    {
        LOG_ERROR("Opened empty file. Exiting...");
        return false;
    }

    LOG_INFO("Successfully opened file "<<filename<<". Total size of file is "<<total_size);
    LOG_INFO("Reading file");

    buf.resize(1300);
    while (fread((void*)buf.c_str(), 1300, 1, f) == 1)
    {
        pos = ftell(f);

        cout <<"\r";
        cout << 100*pos/total_size <<"%";

        _pkt_buf.push_back(buf);

        buf.clear();
        buf.resize(1300);
    }
    cout <<endl;

    fclose(f);
    LOG_INFO("File has successfully read in RAM");

    return true;
}

void FileStreamer::ScheduleStream(const string &tracker_ip, const uint16_t &tracker_port,const uint16_t &stun1_port,
                                  const string &stun2_ip, const uint16_t &stun2_port,const string &stream_name)
{
    _tracker_ip.assign(tracker_ip);
    _tracker_port = tracker_port;
    _stun1_port = stun1_port;
    _stun2_ip.assign(stun2_ip);
    _stun2_port = stun2_port;
    _stream_name.assign(stream_name);


    _connect_task.SetTimeout(0);
    _core->GetScheduler()->Add(&_connect_task);

    _it = _pkt_buf.begin();
    if (!_read_thread->Wake())
    {
        LOG_ERROR("Couldn't wake thread");
    }

}

void FileStreamer::ProcessVideoHeader(const VideoHeader &header)
{
    COTime now;

    _header_parsed = true;
    _start_time = now();
    _cur_dts = 0;
}

void FileStreamer::ProcessVideoPacket(const Packet &packet)
{
    _cur_dts = packet.dts;
}

void FileStreamer::ProcessThread()
{
    uint64_t cur_time;
    string ts_buf;
    while (!_header_parsed)
    {
        EnqueueChunk((*_it));
        if (!_live_reader->Read((*_it)))
        {
            LOG_ERROR("Parsing error "<<COErr::Get());
            return;
        }
        ++_it;
        if (_it == _pkt_buf.end())
        {
            LOG_ERROR("Couldn't parse header");
            _it =_pkt_buf.begin();
            return;
        }
    }

    COTime now;
    cur_time = now()- _start_time;
    //    LOG "cur time "<<cur_time<<", cur_dts "<<_cur_dts EL;

    while(cur_time > _cur_dts)
    {
        EnqueueChunk((*_it));
        if (!_live_reader->Read((*_it)))
        {
            LOG_ERROR("Parsing error "<<COErr::Get());
            return;
        }
        ++_it;
        if (_it == _pkt_buf.end())
        {
            LOG_ERROR("Starting to stream file from the beginning...");
            _start_time = now();
            cur_time = now()-_start_time;
            _cur_dts = 0;
            _it =_pkt_buf.begin();
        }

    }

    if (!ScheduleAdd())
        LOG_ERROR("Couldn't ScheduleAdd(): "<<COErr::Get());
    Utils::Delay(200);
}
