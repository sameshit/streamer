//
//  main.cpp
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "FileStreamer.h"
using namespace StreamerLib;
using namespace CoreObjectLib;
using namespace std;


int main(int argc, char **argv)
{
    CoreObject *_core;
    FileStreamer *file_streamer;
    Variant settings;
    int ret_val;

    ret_val = 0;
    _core = new CoreObject;
    fast_new(file_streamer,_core);

//    file_streamer->PeerSettings()->log_requests = true;

    file_streamer->GetBufferSettings()->buffer_duration = 20000;
    file_streamer->GetBufferSettings()->log_clearing = true;
    if (argc < 2)
    {
        LOG_ERROR("Invalid usage. Right one: filestreamer <path to settings.xml>");
        ret_val = -1;
        goto exit;
    }

    if (!settings.DeserializeXMLFromFile(argv[1]))
    {
        LOG_ERROR("Error parsing xml file("<<argv[1]<<") settings. "<<COErr::Get());
        ret_val = -1;
        goto exit;
    }

    file_streamer->SetCollectStatistic(settings["CollectStatistic"].AsBool());

    if (!file_streamer->Bind(settings["Port"].AsUInt16()))
    {
        LOG_ERROR("Couldn't bind to port "<<settings["BindPort"].AsUInt16());
        ret_val = -1;
        goto exit;
    }

    if (!file_streamer->ReadFile(settings["FileName"].AsChar()))
    {
        LOG_ERROR("Couldn't read file "<<settings["FileName"].AsChar()<<" with reason: "<<COErr::Get());
        ret_val = -1;
        goto exit;
    }

    file_streamer->ScheduleStream(settings["Tracker"]["IP"].AsString(), settings["Tracker"]["Port"].AsUInt16(),
                                  settings["Stun"]["Port1"].AsUInt16(), settings["Stun"]["IP"].AsString(),
                                  settings["Stun"]["Port2"].AsUInt16(), settings["StreamName"].AsString());

    cin.get();

exit:
    fast_delete(file_streamer);
    delete _core;

    return ret_val;
}
