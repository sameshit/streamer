//
//  FileStreamer.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__FileStreamer__
#define __Streamer__FileStreamer__

#include "../src/Streamer.h"

namespace StreamerLib
{
    class FileStreamer
    :public Streamer
    {
    public:
        FileStreamer(CoreObjectLib::CoreObject *core);
        virtual ~FileStreamer();
        
        bool ReadFile(const char* filename);
        void ScheduleStream(const std::string &tracker_ip,const uint16_t &tracker_port,const uint16_t &stun1_port,
                            const std::string &stun2_ip,const uint16_t &stun2_port,const std::string &stream_name);
    private:
        CoreObjectLib::TimeTask _connect_task;
        std::string _tracker_ip,_stun2_ip,_stream_name;
        uint16_t    _tracker_port,_stun1_port,_stun2_port;
        std::list<std::string> _pkt_buf;
        CoreObjectLib::LiveReader *_live_reader;
        bool _header_parsed;
        uint32_t _cur_dts;
        uint64_t _start_time;
        CoreObjectLib::Thread *_read_thread;
        std::list<std::string>::iterator _it;
        
        void ProcessTrackerConnected(MProtoLib::MSession *session);
        void ProcessTrackerDisconnected(MProtoLib::MSession *session);
        void ProcessPeerConnected(MProtoLib::MSession *session);
        void ProcessPeerDisconnected(MProtoLib::MSession *session);
        void ProcessConnect();
        void ProcessVideoPacket(const CoreObjectLib::Packet &packet);
        void ProcessVideoHeader(const CoreObjectLib::VideoHeader &header);
        void ProcessThread();
    };
}

#endif /* defined(__Streamer__FileStreamer__) */
