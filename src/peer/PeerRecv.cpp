//
//  PeerRecv.cpp
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PeerRecv.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

PeerRecv::PeerRecv(CoreObject *core)
:PeerBufferInfo(core)
{

}

PeerRecv::~PeerRecv()
{

}

void PeerRecv::ProcessPeerReliable(MSession *session, uint8_t *data, ssize_t size)
{
    session->GetDisconnectReason() << "Received reliable message from peer";
    Disconnect(session);
}

void PeerRecv::ProcessPeerNonReliable(MSession *session, uint8_t *data, ssize_t size)
{
    PeerMessageType type;

    if (size < 1)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of peer's non reliable message";
        Disconnect(session);
    }

    type = (PeerMessageType)data[0];
    switch(type)
    {
        case PeerMessageRequest:
            ProcessPeerRequest(session,data+1,size-1);
        break;
        default:
            session->GetDisconnectReason() << "Invalid message type("<<type<<") received while processing non reliable message";
            Disconnect(session);
        break;
    }
}
