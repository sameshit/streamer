//
//  BufferInfo.cpp
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PeerBufferInfo.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

PeerBufferInfo::PeerBufferInfo(CoreObject *core)
:PeerBase(core)
{
    OnBufferCleared.Attach(this,&PeerBufferInfo::SendBufferInfo);
}

PeerBufferInfo::~PeerBufferInfo()
{

}

ssize_t PeerBufferInfo::GenerateBufferInfo()
{
    uint8_t *pos;
    ssize_t size;
    uint32_t duration;
    ssize_t i;

    pos = _buffer_info_data;
    Utils::PutByte(pos, (int)PeerMessageBufferInfo);    ++pos;

    size = 1;
    duration = 1001;
    i = 0;
    for (auto it = _buffer.begin(); it != _buffer.end(); ++it)
    {
        duration += (*it).second.duration;
        if (duration > 2*1000)
        {
            Utils::PutBe32(pos, (*it).first);
            pos += 4;
            size += 4;
            duration = 0;
        }
        ++i;
    }

    Utils::PutBe32(pos, (*_buffer.rbegin()).first);
    size+=4;

    return size;
}

void PeerBufferInfo::SendBufferInfo()
{
    MSession *session;
    ssize_t size;

    if (_buffer.begin() == _buffer.end())
        return;

    size = GenerateBufferInfo();
    for (auto it = SessionsBegin(); it != SessionsEnd(); ++it)
    {
        session = (*it).second;
        if (session != GetTrackerSession())
            SendNonReliable(session, _buffer_info_data, size);
    }
}

void PeerBufferInfo::SendBufferInfo(MSession *session)
{
    ssize_t size;

    if (_buffer.begin() == _buffer.end())
        return;

    size = GenerateBufferInfo();
    SendNonReliable(session, _buffer_info_data,size);
}
