//
//  PeerRequest.cpp
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "PeerRequest.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

PeerRequest::PeerRequest(CoreObject *core)
:PeerRecv(core)
{

}

PeerRequest::~PeerRequest()
{

}

void PeerRequest::ProcessPeerRequest(MSession *session, uint8_t *data, ssize_t size)
{
    uint32_t chunk_id;
    BufferMapIterator buf_it;
    PeerMessageType type;
    uint8_t be_id[4];
    ssize_t remaining_size, i;
    uint8_t *pos;
    bool if_chunk_set;

    if (size < 4)
    {
        session->GetDisconnectReason() << "Invalid size("<<size<<") of Request message.";
        Disconnect(session);
        return;
    }

    type = PeerMessageResponse;
    chunk_id = Utils::GetBe32(data);
    pos = data + 4;
    remaining_size = size - 4;
    i = 0;

    do
    {
        buf_it = _buffer.find(chunk_id);
        if (buf_it != _buffer.end())
        {
            Utils::PutBe32(be_id, chunk_id);
            SendNonReliable(session, (uint8_t*)&type, 1, be_id, 4,
                            (uint8_t*)(*buf_it).second.data.c_str(), 1300);

            if (GetPeerSettings()->log_requests)
                LOG_INFO("Responsed chunk with id "<<chunk_id);
        }
        else if (GetPeerSettings()->log_requests)
            LOG_INFO("Requested chunk("<<chunk_id<<") that I don't have. Skipping...");

        if_chunk_set = false;
        while (remaining_size > 0 && !if_chunk_set)
        {
            if_chunk_set = *pos & (1 << (i%8));
            ++i;
            ++chunk_id;

            if (i == 8)
            {
                --remaining_size;
                i = 0;
                pos += 1;
            }
        }
    }
    while (remaining_size > 0);
}
