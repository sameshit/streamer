//
//  PeerBase.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef Streamer_PeerBase_h
#define Streamer_PeerBase_h

#include "../buffer/BufferClearing.h"

namespace StreamerLib
{
    enum PeerMessageType
    {
        PeerMessageBufferInfo  = 1,
        PeerMessageRequest,
        PeerMessageResponse,
    };
    
    class PeerSettings
    {
    public:
        PeerSettings():log_requests(false) {}
        virtual ~PeerSettings() {}
        
        bool log_requests;
    };
    
    enum PeerState
    {
        PeerNotAuthed,
        PeerAuthed,
    };
    
    class PeerInfo
    {
    public:
        PeerInfo():peer_state(PeerNotAuthed) {}
        virtual ~PeerInfo() {}
        
        PeerState peer_state;
        uint64_t real_addr;
    };
#define GET_PEER_INFO(ses) ((PeerInfo*)ses->GetUserData())
    
    class PeerBase
    :public BufferClearing
    {
    public:
        PeerBase(CoreObjectLib::CoreObject *core):BufferClearing(core){}
        virtual ~PeerBase(){}
        
        PeerSettings* GetPeerSettings() {return &_peer_settings;}
    private:
        class PeerSettings _peer_settings;
    };
}

#endif
