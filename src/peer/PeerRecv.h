//
//  PeerRecv.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__PeerRecv__
#define __Streamer__PeerRecv__

#include "PeerBufferInfo.h"

namespace StreamerLib
{
    class PeerRecv
    :public PeerBufferInfo
    {
    public:
        PeerRecv(CoreObjectLib::CoreObject *core);
        virtual ~PeerRecv();
    private:
        MPROTO_MESSAGE_PROC(ProcessPeerReliable);
        MPROTO_MESSAGE_PROC(ProcessPeerNonReliable);
        
        virtual MPROTO_MESSAGE_PROC(ProcessPeerRequest) = 0;
        
        friend class PeerRequest;
    };
}

#endif /* defined(__Streamer__PeerRecv__) */
