//
//  PeerRequest.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__PeerRequest__
#define __Streamer__PeerRequest__

#include "PeerRecv.h"

namespace StreamerLib
{
    class PeerRequest
    :public PeerRecv
    {
    public:
        PeerRequest(CoreObjectLib::CoreObject *core);
        virtual ~PeerRequest();
    private:
        void ProcessPeerRequest(MProtoLib::MSession *session, uint8_t *data, ssize_t size);
    };
}

#endif /* defined(__Streamer__PeerRequest__) */