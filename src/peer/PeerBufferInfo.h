//
//  BufferInfo.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__BufferInfo__
#define __Streamer__BufferInfo__

#include "PeerBase.h"

namespace StreamerLib
{
    class PeerBufferInfo
    :public PeerBase
    {
    public:
        PeerBufferInfo(CoreObjectLib::CoreObject *core);
        virtual ~PeerBufferInfo();
    private:
        void SendBufferInfo();
        MPROTO_CONNECT_PROC(SendBufferInfo);
        ssize_t GenerateBufferInfo();        
        uint8_t _buffer_info_data[1300];
    };
}

#endif /* defined(__Streamer__BufferInfo__) */
