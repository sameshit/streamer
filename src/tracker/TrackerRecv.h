//
//  TrackerRecv.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerRecv__
#define __Streamer__TrackerRecv__

#include "TrackerBase.h"

namespace StreamerLib
{
    const uint32_t kTrackerProtocolVersion = 4;

    enum MTrackerMessageType
    {
        MTReqVersion              =1,
        MTRepInvalidVersion         ,

        MTReqCreateStream           ,
        MTReqCloseStream            ,
        MTReqPlayStreamById         ,
        MTReqPlayStreamByName       ,
        MTReqGetAllStreams          ,
        MTReqStopStream             ,
        MTReqLocalNatInfo           ,
        MTReqStatistic              ,

        MTRepStunConnect            ,
        MTRepAllStreams             ,
        MTRepStreamer               ,
        MTRepNeighborChange         ,
        MTRepStreamClosed           ,
        MTRepRealAddr               ,
        MTRepSuperPeerEnable        ,
        MTRepSuperPeerDisable       ,
        MTRepStatistic              ,
    };

    typedef CoreObjectLib::Event<> StreamClosedEvent;

    class TrackerRecv
    :public TrackerBase
    {
    public:
        TrackerRecv(CoreObjectLib::CoreObject *core);
        virtual ~TrackerRecv();

        uint8_t *GetRealAddr();
        StreamClosedEvent OnStreamClosed;
    private:
        void ProcessTrackerMessage(MProtoLib::MSession *session, uint8_t *data, ssize_t size);
        void ProcessRealAddr      (uint8_t *data, ssize_t size);
        void ProcessStreamClosed  (uint8_t *data, ssize_t size);
        virtual void StunPhase2(uint8_t *data,ssize_t size) = 0;
        virtual void ProcessInvalidVersion(uint8_t *data,ssize_t size) = 0;
        uint64_t _real_addr;

        friend class TrackerStun;
    };

    inline uint8_t* TrackerRecv::GetRealAddr()
    {
        return (uint8_t *)&_real_addr;
    }
}

#endif /* defined(__Streamer__TrackerRecv__) */
