//
//  TrackerStun.h
//  Streamer
//
//  Created by Oleg on 21.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerStun__
#define __Streamer__TrackerStun__

#include "TrackerVersion.h"

namespace StreamerLib
{
    class TrackerStun
    :public TrackerVersion
    {
    public:
        TrackerStun(CoreObjectLib::CoreObject *core);
        virtual ~TrackerStun();
    private:
        MPROTO_CONNECT_PROC(StunConnected);
        MPROTO_DISCONNECT_PROC(StunDisconnected);
        void StunPhase2(uint8_t *data,ssize_t size);
    };
}

#endif /* defined(__Streamer__TrackerStun__) */
