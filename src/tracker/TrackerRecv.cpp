//
//  TrackerRecv.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerRecv.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;

TrackerRecv::TrackerRecv(CoreObject *core)
:TrackerBase(core)
{
    
}

TrackerRecv::~TrackerRecv()
{

}

void TrackerRecv::ProcessTrackerMessage(MProtoLib::MSession *session, uint8_t *data, ssize_t size)
{
    MTrackerMessageType type;
    
    if (size < 1)
    {
        session->GetDisconnectReason() << "FATAL: Invalid size("<<size<<") of tracker message!";
        Disconnect(session);
        return;
    }
    
    type = (MTrackerMessageType)data[0];
    switch (type)
    {
        case MTRepRealAddr:
            ProcessRealAddr(data + 1, size - 1);
        break;
        case MTRepStreamClosed:
            ProcessStreamClosed(data + 1, size - 1);
        break;
        case MTRepStunConnect:
            StunPhase2(data+1, size-1);
        break;
        case MTRepInvalidVersion:
            ProcessInvalidVersion(data+1, size-1);
        break;
        default:
            session->GetDisconnectReason() << "FATAL: Unknown message("<<type<<") received from tracker!";
            Disconnect(session);
        break;
    }
}

void TrackerRecv::ProcessRealAddr(uint8_t *data, ssize_t size)
{
    if (size != 8)
    {
        _tracker_session->GetDisconnectReason() << "FATAL: Invalid size("<<size<<") in TrackerRecv::ProcessRealAddr()!";
        Disconnect(_tracker_session);
        return;
    }
    
    memcpy ((uint8_t*)&_real_addr,data,size);
}

void TrackerRecv::ProcessStreamClosed(uint8_t *data, ssize_t size)
{
    _tracker_state = TrackerConnected;
    DisconnectAllNeighbors("Stream closed by tracker");
    OnStreamClosed();
}