//
//  TrackerStun.cpp
//  Streamer
//
//  Created by Oleg on 21.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerStun.h"

using namespace StreamerLib;
using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace std;

TrackerStun::TrackerStun(CoreObject *core)
:TrackerVersion(core)
{

}

TrackerStun::~TrackerStun()
{

}

void TrackerStun::StunConnected(MSession *session)
{
    SendReliable(session, (uint8_t*)&_real_addr, sizeof(uint64_t));

    LOG_INFO("Stun server "<<PRINT_SESSION_ADDR(session)<< " connected");
}

void TrackerStun::StunDisconnected(MSession *session)
{
    LOG_INFO("Stun server "<<PRINT_SESSION_ADDR(session)<< " disconnected with reason: "
    <<session->GetDisconnectReason().str());
}

void TrackerStun::StunPhase2(uint8_t *data, ssize_t size)
{
    struct sockaddr_in addr;
    if (size != 0)
    {
        _tracker_session->GetDisconnectReason() << "Invalid size("<<size <<") of StunConnect message";
        Disconnect(_tracker_session);
        return;
    }

    MSession::UnPackAddress((uint8_t*)&_stun2_addr, &addr);
    Connect(&addr);
}
