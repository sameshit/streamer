//
//  TrackerStreamerFunctions.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerStreamerFunctions.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

TrackerStreamerFunctions::TrackerStreamerFunctions(CoreObject *core)
:TrackerLocalNatInfo(core),_stream_created(false),_collect_statistic(false)
{
    _create_stream_task.SetTimeout(0);
    _create_stream_task.OnTimeTask.Attach(this, &TrackerStreamerFunctions::LoopCreateStream);
    _close_stream_task.SetTimeout(0);
    _close_stream_task.OnTimeTask.Attach(this, &TrackerStreamerFunctions::LoopCloseStream);
}

TrackerStreamerFunctions::~TrackerStreamerFunctions()
{
    
}

bool TrackerStreamerFunctions::CreateStream(const char *stream_name)
{
    if (_tracker_state != TrackerConnected)
    {
        COErr::Set("Invalid tracker state");
        return false;
    }
    
    _stream_name.assign(stream_name);
    _tracker_state = TrackerStreaming;
    if (_core->IsLoopThread())
        LoopCreateStream();
    else
        _core->GetScheduler()->Add(&_create_stream_task);    
    return true;
}

void TrackerStreamerFunctions::LoopCreateStream()
{
    uint8_t *data;
    ssize_t len;
    
    len = _stream_name.size();
    fast_alloc(data, 2 + len);
    Utils::PutByte(data, MTReqCreateStream);
    data[1] = _collect_statistic?1:0;
    memcpy(data + 2, _stream_name.c_str(),len);
    SendReliable(_tracker_session, data, len + 2);
    fast_free(data);
    
    _stream_created = true;
    
    OnCreateStream();
}

bool TrackerStreamerFunctions::CloseStream()
{
    if (_tracker_state != TrackerStreaming)
    {
        COErr::Set("Invalid peer state");
        return false;
    }

    _tracker_state = TrackerConnected;
    if (_core->IsLoopThread())
        LoopCloseStream();
    else
        _core->GetScheduler()->Add(&_close_stream_task);
    
    return true;
}

void TrackerStreamerFunctions::LoopCloseStream()
{
    uint8_t data[1];
    Utils::PutByte(data, MTReqCloseStream);
    SendReliable(_tracker_session, data, 1);
    
    _stream_created = false;
    DisconnectAllNeighbors("Stream closed");
    
    OnCloseStream();
}

void TrackerStreamerFunctions::CloseStreamBeforeDisconnect()
{
    if (_stream_created)
    {
        _stream_created = false;
        OnCloseStream();
    }
}