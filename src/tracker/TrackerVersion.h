//
//  TrackerVersion.h
//  Streamer
//
//  Created by Oleg on 21.06.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerVersion__
#define __Streamer__TrackerVersion__

#include "TrackerRecv.h"

namespace StreamerLib
{
    typedef CoreObjectLib::Event<> InvalidVersionEvent;

    class TrackerVersion
    :public TrackerRecv
    {
    public:
        TrackerVersion(CoreObjectLib::CoreObject *core);
        virtual ~TrackerVersion();

        InvalidVersionEvent OnInvalidVersion;
    private:
        void ProcessInvalidVersion(uint8_t *data,ssize_t size);
        void SendTrackerProtocolVersion();
    };
}

#endif /* defined(__Streamer__TrackerVersion__) */
