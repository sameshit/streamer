//
//  StreamerTracker.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__StreamerTracker__
#define __Streamer__StreamerTracker__

#include "TrackerStreamerFunctions.h"

namespace StreamerLib
{
    class StreamerTracker
    :public TrackerStreamerFunctions
    {
    public:
        StreamerTracker(CoreObjectLib::CoreObject *core);
        virtual ~StreamerTracker();
    };
}

#endif /* defined(__Streamer__StreamerTracker__) */
