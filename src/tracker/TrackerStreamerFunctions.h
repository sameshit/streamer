//
//  TrackerStreamerFunctions.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerStreamerFunctions__
#define __Streamer__TrackerStreamerFunctions__

#include "TrackerLocalNatInfo.h"

namespace StreamerLib
{
    typedef CoreObjectLib::Event<>   CreateStreamEvent;
    typedef CoreObjectLib::Event<>   CloseStreamEvent;


    class TrackerStreamerFunctions
    :public TrackerLocalNatInfo
    {
    public:
        TrackerStreamerFunctions(CoreObjectLib::CoreObject *core);
        virtual ~TrackerStreamerFunctions();

        bool CreateStream   (const char *stream_name = NULL);
        bool CreateStream   (const std::string &stream_name);
        bool CloseStream    ();

        inline void SetCollectStatistic(const bool &collect_statistic) {_collect_statistic = collect_statistic;}

        CreateStreamEvent   OnCreateStream;
        CloseStreamEvent    OnCloseStream;
    private:
        bool _stream_created;
        CoreObjectLib::TimeTask _create_stream_task,_close_stream_task;
        std::string _stream_name;
        bool _collect_statistic;

        void LoopCreateStream();
        void LoopCloseStream();
        void CloseStreamBeforeDisconnect();
    };

    inline bool TrackerStreamerFunctions::CreateStream(const std::string &stream_name)
    {
        return CreateStream(stream_name.c_str());
    }
}

#endif /* defined(__Streamer__TrackerStreamerFunctions__) */
