//
//  TrackerBase.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__TrackerBase__
#define __Streamer__TrackerBase__

#include "../../MProto/src/MProto.h"

namespace StreamerLib
{
    enum TrackerState
    {
        TrackerNotConnected,
        TrackerConnecting,
        TrackerConnected,
        TrackerStreaming,
    };
        
    class TrackerBase
    :public MProtoLib::MProto
    {
    public:
        TrackerBase(CoreObjectLib::CoreObject *core);
        virtual ~TrackerBase();
        
        bool                        ConnectToTracker(const char *tracker_ip, const uint16_t &tracker_port,
                                                     const uint16_t &stun1_port, const char *stun2_ip,
                                                     const uint16_t &stun2_port);
        
        inline bool                 ConnectToTracker(const std::string &tracker_ip,const uint16_t &tracker_port,
                                                     const uint16_t &stun1_port,const std::string &stun2_ip,
                                                     const uint16_t &stun2_port)
                                                    {return ConnectToTracker(tracker_ip.c_str(),
                                                                             tracker_port,stun1_port,
                                                                             stun2_ip.c_str(),
                                                                             stun2_port);}
        MProtoLib::MSession*        GetTrackerSession();
        
        MProtoLib::ConnectEvent     OnTrackerConnect,OnPeerConnect;
        MProtoLib::DisconnectEvent  OnTrackerDisconnect,OnPeerDisconnect;
    private:
        MProtoLib::MSession *_tracker_session;
        TrackerState _tracker_state;
        std::string _ip;
        uint16_t _port;
        CoreObjectLib::TimeTask _connect_tracker_task;
        uint64_t _stun1_addr,_stun2_addr;
        
        MPROTO_MESSAGE_VPROC(ProcessPeerReliable);
        MPROTO_MESSAGE_VPROC(ProcessPeerNonReliable);
        MPROTO_MESSAGE_VPROC(ProcessTrackerMessage);
        MPROTO_CONNECT_VPROC(SendBufferInfo);
        MPROTO_CONNECT_VPROC(StunConnected);
        MPROTO_DISCONNECT_VPROC(StunDisconnected);
        
        virtual void CloseStreamBeforeDisconnect() = 0;

        void LoopConnectToTracker();
        void DisconnectAllNeighbors(const char *reason);
        void ProcessSessionConnected(MProtoLib::MSession *session);
        void ProcessSessionDisconnected(MProtoLib::MSession *session);
        void ProcessSessionMessage(MProtoLib::MSession *session,uint8_t *data,ssize_t size);
        void ProcessSessionNonReliableMessage(MProtoLib::MSession *session,uint8_t *data,ssize_t size);
        friend class TrackerRecv;
        friend class TrackerStreamerFunctions;
        friend class TrackerStun;
    protected:
        virtual void SendTrackerProtocolVersion() = 0;
    };
    
    inline void TrackerBase::ProcessSessionMessage(MProtoLib::MSession *session, uint8_t *data, ssize_t size)
    {
        if (session == _tracker_session)
            ProcessTrackerMessage(session,data,size);
        else
            ProcessPeerReliable(session,data,size);
    }
    
    inline void TrackerBase::ProcessSessionNonReliableMessage(MProtoLib::MSession *session,uint8_t *data,ssize_t size)
    {
        if (session == _tracker_session)
        {
            session->GetDisconnectReason() << "Received non reliable message from tracker!";
            Disconnect(session);
        }
        else
            ProcessPeerNonReliable(session,data,size);
    }
    
    inline MProtoLib::MSession* TrackerBase::GetTrackerSession()
    {
        return _tracker_session;
    }
}

#endif /* defined(__Streamer__TrackerBase__) */
