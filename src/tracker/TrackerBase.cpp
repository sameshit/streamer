//
//  TrackerBase.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "TrackerBase.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

TrackerBase::TrackerBase(CoreObject *core)
:MProto(core),_tracker_session(NULL),_tracker_state(TrackerNotConnected)
{
    OnConnect.              Attach      (this, &TrackerBase::ProcessSessionConnected);
    OnDisconnect.           Attach      (this, &TrackerBase::ProcessSessionDisconnected);
    OnReliableMessage.      Attach      (this, &TrackerBase::ProcessSessionMessage);
    OnNonReliableMessage.   Attach      (this, &TrackerBase::ProcessSessionNonReliableMessage);

    _connect_tracker_task.SetTimeout(0);
    _connect_tracker_task.OnTimeTask.Attach(this, &TrackerBase::LoopConnectToTracker);
}

TrackerBase::~TrackerBase()
{

}

bool TrackerBase::ConnectToTracker(const char *tracker_ip, const uint16_t &tracker_port,
                                   const uint16_t &stun1_port, const char* stun2_ip,
                                   const uint16_t &stun2_port)
{
    if (_tracker_state != TrackerNotConnected)
    {
        COErr::Set("Already connected to tracker");
        return false;
    }

    _tracker_state = TrackerConnecting;
    MSession::MakePackedAddress(&_stun1_addr,tracker_ip,stun1_port);
    MSession::MakePackedAddress(&_stun2_addr,stun2_ip,stun2_port);
    _ip.assign(tracker_ip);
    _port = tracker_port;
    if (_core->IsLoopThread())
        LoopConnectToTracker();
    else
        _core->GetScheduler()->Add(&_connect_tracker_task);

    return true;
}

void TrackerBase::LoopConnectToTracker()
{
    Connect(_ip.c_str(),_port);
}

void TrackerBase::ProcessSessionConnected(MProtoLib::MSession *session)
{
    if (_tracker_session == NULL)
    {
        if (_tracker_state != TrackerConnecting)
        {
            session->GetDisconnectReason() << "Someone connected while I'm not connected to tracker";
            Disconnect(session);
            return;
        }
        else
        {
            _tracker_session = session;
            _tracker_state = TrackerConnected;
            SendTrackerProtocolVersion();
            OnTrackerConnect(session);
        }
    }
    else
    {
        if (session->GetPackedAddr() == _stun1_addr || session->GetPackedAddr() == _stun2_addr)
            StunConnected(session);
        else
        if (_tracker_state != TrackerStreaming)
        {
            session->GetDisconnectReason() << "Someone connected while I'm not streaming";
            Disconnect(session);
            return;
        }
        else
        {
            SendBufferInfo(session);
            OnPeerConnect(session);
        }
    }
}

void TrackerBase::ProcessSessionDisconnected(MProtoLib::MSession *session)
{
    if (_tracker_state != TrackerNotConnected)
    {
        if (session == _tracker_session)
        {
            DisconnectAllNeighbors("Tracker connection lost");
            _tracker_state = TrackerNotConnected;
            CloseStreamBeforeDisconnect();
            OnTrackerDisconnect(session);
            _tracker_session = NULL;
        }
        else if (session->GetPackedAddr() == _stun1_addr || session->GetPackedAddr() == _stun2_addr)
            StunDisconnected(session);
        else
            OnPeerDisconnect(session);
    }
}

void TrackerBase::DisconnectAllNeighbors(const char *reason)
{
    MSessionMapIterator it,next_it;

    for (it = SessionsBegin(),next_it = it==SessionsEnd()?SessionsEnd():it ++;
         it != SessionsEnd();
         )
    {
        if ((*it).second != _tracker_session)
        {
            (*it).second->GetDisconnectReason() << reason;
            Disconnect(it);
        }

        it=next_it;
        if (next_it != SessionsEnd())
            ++next_it;
    }
}
