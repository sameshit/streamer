//
//  BufferAdding.cpp
//  Streamer
//
//  Created by Oleg on 01.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferAdding.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

BufferAdding::BufferAdding(CoreObject *core)
:BufferBase(core),_last_buffer_id(0),_buffer_duration(0)
{
    fast_new(_live_reader,_core);
    fast_new(_schedule_sem,1);
    
    OnCloseStream.Attach(this,&BufferAdding::ProcessStreamClosed);
    _live_reader->OnVideoPacket.Attach(this, &BufferAdding::ProcessVideoPacket);
    
    _add_task.OnTimeTask.Attach(this, &BufferAdding::ProcessAdd);
}

BufferAdding::~BufferAdding()
{    
    fast_delete(_live_reader);
    fast_delete(_schedule_sem);
}

bool BufferAdding::EnqueueChunk(const std::string &chunk)
{
    if (chunk.size() != kChunkSize)
    {
        COErr::Set("Invalid size of chunk");
        return false;
    }
    
    {
        ScopedSpin lock(_enqueue_lock);
        _chunk_q.push(chunk);
    }
    return true;
}

bool BufferAdding::ScheduleAdd()
{
    if (!_schedule_sem->Wait())
        return false;
    
    if (!_core->GetScheduler()->Add(&_add_task))
        return false;
    
    return true;
}

bool BufferAdding::AddChunk(const std::string &chunk)
{
    Chunk new_chunk;
    uint32_t old_duration;
    
    if (chunk.size() != kChunkSize)
    {
        COErr::Set("Invalid size of chunk");
        return false;
    }
    
    
    ++_last_buffer_id;
    old_duration = _buffer_duration;
    if (!_live_reader->Read(chunk))
    {
        _live_reader->Reset();
        _live_reader->Read(chunk);
    }
    new_chunk.data.assign(chunk);
    new_chunk.duration = _buffer_duration - old_duration;
    _buffer.insert(make_pair(_last_buffer_id, new_chunk));
    
    return true;
}

void BufferAdding::ProcessStreamClosed()
{        
        _buffer.clear();
        _last_buffer_id = 0;
        _buffer_duration = 0;
}

void BufferAdding::ProcessVideoPacket(const CoreObjectLib::Packet &packet)
{
    _buffer_duration += packet.duration;
}

void BufferAdding::ProcessAdd()
{
    {
        ScopedSpin lock(_enqueue_lock);
        
        while (!_chunk_q.empty())
        {
            AddChunk(_chunk_q.front());
            _chunk_q.pop();
        }
    }
    
    _schedule_sem->Post();
}