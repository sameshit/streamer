//
//  BufferAdding.h
//  Streamer
//
//  Created by Oleg on 01.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__BufferAdding__
#define __Streamer__BufferAdding__

#include "BufferBase.h"
#include "../../MProto/CoreObject/src/utils/live/LiveReader.h"

namespace StreamerLib
{
    class Chunk
    {
    public:
        Chunk() {}
        virtual ~Chunk() {}

        uint32_t duration;
        std::string data;
    };

    const size_t kChunkSize = 1300;
    typedef std::map<uint32_t,Chunk,MProtoLib::OrderIntIdLess>             BufferMap;
    typedef std::map<uint32_t,Chunk,MProtoLib::OrderIntIdLess>::iterator   BufferMapIterator;

    class BufferAdding
    :public BufferBase
    {
    public:
        BufferAdding(CoreObjectLib::CoreObject *core);
        virtual ~BufferAdding();

        bool ScheduleAdd();
        bool AddChunk(const std::string &chunk);
        bool EnqueueChunk(const std::string &chunk);
    private:
        uint32_t                    _last_buffer_id;
        BufferMap                   _buffer;
        CoreObjectLib::Semaphore    *_schedule_sem;
        CoreObjectLib::LiveReader   *_live_reader;
        CoreObjectLib::SpinLock     _enqueue_lock;
        uint32_t                    _buffer_duration;
        std::queue<std::string>     _chunk_q;
        CoreObjectLib::TimeTask     _add_task;
        CoreObjectLib::Event<>      OnBufferCleared;

        void ProcessAdd();
        void ProcessStreamClosed();
        void ProcessVideoPacket(const CoreObjectLib::Packet &packet);

        friend class BufferClearing;
        friend class PeerBufferInfo;
        friend class PeerRequest;
        // friend class is used instead of protected, as noone except this three classes
        // may access the buffer
    };
}

#endif /* defined(__Streamer__BufferAdding__) */
