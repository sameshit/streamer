//
//  BufferClearing.cpp
//  Streamer
//
//  Created by Oleg on 01.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "BufferClearing.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

BufferClearing::BufferClearing(CoreObject *core)
:BufferAdding(core)
{
    fast_new(_clear_timer, _core, 3000);

    OnCreateStream.Attach(this,&BufferClearing::ProcessStreamCreate);
    OnCloseStream.Attach(this,&BufferClearing::ProcessStreamClose);
}

BufferClearing::~BufferClearing()
{
    fast_delete(_clear_timer);
}

void BufferClearing::ProcessClear()
{
    uint32_t cleared =0;

    if (GetBufferSettings()->log_clearing)
        LOG_INFO("Clearing buffer... Current duration is "<<_buffer_duration);

    while (_buffer_duration > GetBufferSettings()->buffer_duration)
    {
        auto it = _buffer.begin();

        _buffer_duration -= (*it).second.duration;
        cleared += kChunkSize;
        _buffer.erase(it);
    }

    if (GetBufferSettings()->log_clearing)
        LOG_INFO("Buffer cleared. Current duration is "<<_buffer_duration<<". Cleared: "<<cleared/1024<<"kb");

    OnBufferCleared();
}

void BufferClearing::ProcessStreamCreate()
{
    _event_id = _clear_timer->OnTimer.Attach(this, &BufferClearing::ProcessClear);
}

void BufferClearing::ProcessStreamClose()
{
    _clear_timer->OnTimer.Deattach(_event_id);
    _buffer.clear();
    _buffer_duration = 0;
}
