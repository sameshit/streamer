//
//  StreamerBase.h
//  Streamer
//
//  Created by Oleg on 01.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef Streamer_StreamerBase_h
#define Streamer_StreamerBase_h

#include "../tracker/StreamerTracker.h"

namespace StreamerLib
{
    class BufferSettings
    {
    public:
        bool        log_clearing;
        uint32_t    buffer_duration;
        
        BufferSettings() :log_clearing(false),buffer_duration(10000) {}
        virtual ~BufferSettings(){}
    };
    
    class BufferBase
    :public StreamerTracker
    {
    public:
        BufferBase(CoreObjectLib::CoreObject *core):StreamerTracker(core) {}
        virtual ~BufferBase() {}
        
        BufferSettings* GetBufferSettings() {return &_buffer_settings;}
    private:
        class BufferSettings _buffer_settings;
    };
}

#endif
