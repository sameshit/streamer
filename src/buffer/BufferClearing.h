//
//  BufferClearing.h
//  Streamer
//
//  Created by Oleg on 01.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__BufferClearing__
#define __Streamer__BufferClearing__

#include "BufferAdding.h"

namespace StreamerLib
{
    class BufferClearing
    :public BufferAdding
    {
    public:
        BufferClearing(CoreObjectLib::CoreObject *core);
        virtual ~BufferClearing();
    private:
        CoreObjectLib::Timer *_clear_timer;
        uint32_t _event_id;
        
        uint32_t GetAdviceId();
        void ProcessStreamCreate();
        void ProcessStreamClose();
        void ProcessClear();
    };
}

#endif /* defined(__Streamer__BufferClearing__) */
