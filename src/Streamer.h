//
//  Streamer.h
//  Streamer
//
//  Created by Oleg on 02.04.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__Streamer__
#define __Streamer__Streamer__

#include "peer/PeerRequest.h"

namespace StreamerLib
{
    class LIBEXPORT Streamer
    :public PeerRequest
    {
    public:
        Streamer(CoreObjectLib::CoreObject *core):PeerRequest(core) {}
        virtual ~Streamer() {}
    };
}

#endif /* defined(__Streamer__Streamer__) */
