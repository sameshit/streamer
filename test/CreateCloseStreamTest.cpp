//
//  CreateCloseStreamTest.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "CreateCloseStreamTest.h"

using namespace CoreObjectLib;
using namespace MProtoLib;
using namespace StreamerLib;
using namespace std;

CreateCloseStreamTest::CreateCloseStreamTest()
{
    _core = new CoreObject;
    fast_new1(StreamerTracker, _streamer, _core);
    
    _streamer->OnPeerConnect.Attach(this, &CreateCloseStreamTest::ProcessPeerConnected);
    _streamer->OnPeerDisconnect.Attach(this, &CreateCloseStreamTest::ProcessPeerDisconnected);
    _streamer->OnTrackerConnect.Attach(this, &CreateCloseStreamTest::ProcessTrackerConnected);
    _streamer->OnTrackerDisconnect.Attach(this, &CreateCloseStreamTest::ProcessTrackerDisconnected);
}

CreateCloseStreamTest::~CreateCloseStreamTest()
{
    fast_delete(StreamerTracker, _streamer);
    delete _core;
}

bool CreateCloseStreamTest::Run()
{
    if (!_streamer->Bind(12475))
    {
        COErr::Set("couldn't bind");
        return false;
    }
    
    _streamer->ConnectToTracker("127.0.0.1", 12345);
    
    if (!_streamer->CreateStream())
    {
        COErr::Set("couldn't create stream");
        return false;
    }
    
    cin.get();
    
    if (!_streamer->CloseStream())
    {
        COErr::Set("couldn't close stream");
        return false;
    }
    
    return true;
}

void CreateCloseStreamTest::ProcessPeerConnected(MSession *session)
{
    LOG "peer "<<PRINT_SESSION_ADDR(session)<<" connected" EL;
}

void CreateCloseStreamTest::ProcessPeerDisconnected(MSession *session)
{
    LOG "peer "<<PRINT_SESSION_ADDR(session)<< " disconnected with reason: "<<session->GetDisconnectReason().str() EL;
}

void CreateCloseStreamTest::ProcessTrackerConnected(MSession *session)
{
    LOG "connected to tracker "<<PRINT_SESSION_ADDR(session) EL;
}

void CreateCloseStreamTest::ProcessTrackerDisconnected(MSession *session)
{
    LOG "disconnected from tracker "<<PRINT_SESSION_ADDR(session)<<" with reason: "<<session->GetDisconnectReason().str() EL;
}