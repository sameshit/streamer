//
//  CreateCloseStreamTest.h
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#ifndef __Streamer__CreateCloseStreamTest__
#define __Streamer__CreateCloseStreamTest__

#include "../src/tracker/StreamerTracker.h"

namespace StreamerLib
{
    class CreateCloseStreamTest
    {
    public:
        CreateCloseStreamTest();
        virtual ~CreateCloseStreamTest();
        
        bool Run();
    private:
        StreamerTracker *_streamer;
        CoreObjectLib::CoreObject *_core;
       
        void ProcessTrackerConnected(MProtoLib::MSession *session);
        void ProcessTrackerDisconnected(MProtoLib::MSession *session);
        void ProcessPeerConnected(MProtoLib::MSession *session);
        void ProcessPeerDisconnected(MProtoLib::MSession *session);
    };
}

#endif /* defined(__Streamer__CreateCloseStreamTest__) */
