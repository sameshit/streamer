//
//  main.cpp
//  Streamer
//
//  Created by Oleg on 31.03.13.
//  Copyright (c) 2013 Oleg. All rights reserved.
//

#include "CreateCloseStreamTest.h"

using namespace CoreObjectLib;
using namespace StreamerLib;
using namespace std;

int main (int argc, char **argv)
{
    CreateCloseStreamTest test;
    
    if (!test.Run())
        cout<< "test failed: "<<COErr::Get() <<endl;

    cin.get();
    
    return 0;
}
